#include <iostream>
#include <vector>
#include <fstream>
#include <unordered_map>
#include <unordered_set>
#include <sstream>


using namespace std;

class instruction {
public:
    string label = "";
    string opcode = "";
    vector<string> leftOperands;
    vector<string> rightOperands;
    int lineNumber = 0;
};

//map label to lineNum
unordered_map<string, int> label2line;


void ParseInstructions(string filename, vector<instruction> &instructions) {
    ifstream infile;
    infile.open(filename);
    string line = "";
    int lineNumber = 0;
    while (getline(infile, line)) {
        //if there is comment, filter the comment
        if (line.find("/") != string::npos) {
            for (int i = 0; i < line.length(); i++) {
                if (line[i] == '/') {
                    line = line.substr(0, i);
                    break;
                }
            }
        }

        instruction instr;
        lineNumber += 1;
        instr.lineNumber = lineNumber;
        if (line.find(":") != string::npos) {
            for (int i = 0; i < line.length(); i++) {
                if (line[i] == ':') {
                    string labelName = line;
                    labelName = labelName.substr(0, i);
                    int length = line.length();
                    line = line.substr(i + 1, length - i);
                    instr.label = labelName;
                    instr.opcode = "nop";
                    instructions.push_back(instr);
                    label2line[instr.label] = instr.lineNumber;
                }
            }
        }
        else {
            int i = 0;
            while (isalpha(line[i]) && i < line.length()) {
                i++;
            }
            i++;
            for (int j = i; j < line.length(); j++) {
                string opcode = "";
                opcode += line[j++];
                while ((isalnum(line[j]) || line[j] == '_') && j < line.length()) {
                    opcode += line[j];
                    j++;
                }
                instr.opcode = opcode;
                //end of file
                if (opcode == "halt") {
                    instructions.push_back(instr);
                    return;
                }
                else if (opcode == "output" || opcode == "coutput" || opcode == "write" || opcode == "cwrite") {
                    istringstream iss(line);
                    vector<string> tokens;
                    copy(istream_iterator<string>(iss),
                         istream_iterator<string>(),
                         back_inserter(tokens));

                    instr.leftOperands.push_back(tokens[1]);
                    instructions.push_back(instr);
                    break;
                }
                else if (opcode == "br" || opcode == "read" || opcode == "cread") {
                    istringstream iss(line);
                    vector<string> tokens;
                    copy(istream_iterator<string>(iss),
                         istream_iterator<string>(),
                         back_inserter(tokens));

                    instr.rightOperands.push_back(tokens[2]);
                    instructions.push_back(instr);
                    break;
                }
                else if (opcode == "cbr" || opcode == "storeAI" || opcode == "storeAO" || opcode == "cstoreAI" || opcode == "cstoreAO") {
                    istringstream iss(line);
                    vector<string> tokens;
                    copy(istream_iterator<string>(iss),
                         istream_iterator<string>(),
                         back_inserter(tokens));

                    instr.leftOperands.push_back(tokens[1]);
                    instr.rightOperands.push_back(tokens[3].substr(0, tokens[3].size() - 1));
                    instr.rightOperands.push_back(tokens[4]);
                    instructions.push_back(instr);
                    break;
                }

                else if (opcode == "not" || opcode == "loadI" || opcode == "load"
                         || opcode == "cload" || opcode == "store" || opcode == "cstore" || opcode == "i2i"
                         || opcode == "c2c" || opcode == "i2c" || opcode == "c2i"){

                    istringstream iss(line);
                    vector<string> tokens;
                    copy(istream_iterator<string>(iss),
                         istream_iterator<string>(),
                         back_inserter(tokens));

                    instr.leftOperands.push_back(tokens[1]);
                    instr.rightOperands.push_back(tokens[3]);
                    instructions.push_back(instr);
                    break;
                }
                else {
                    istringstream iss(line);
                    vector<string> tokens;
                    copy(istream_iterator<string>(iss),
                         istream_iterator<string>(),
                         back_inserter(tokens));

                    instr.leftOperands.push_back(tokens[1].substr(0, tokens[1].size() - 1));
                    instr.leftOperands.push_back(tokens[2]);
                    instr.rightOperands.push_back(tokens[4]);
                    instructions.push_back(instr);
                    break;
                }
            }
        }

    }
}


void OutputParseResults(vector<instruction> instructions, string outfileName) {
    ofstream outfile;
    outfile.open (outfileName, ios::out);
    for (auto x : instructions) {
        if (x.opcode == "halt") {
            outfile << x.opcode << endl;
        }
        else if (x.opcode == "nop") {
            outfile << x.label << ": " << x.opcode << endl;
        }
        else if (x.opcode == "output" || x.opcode == "coutput" || x.opcode == "write" || x.opcode == "cwrite") {
            outfile << x.opcode << " " << x.leftOperands[0] << endl;
        }
        else if (x.opcode == "read" || x.opcode == "cread") {
            outfile << x.opcode << " => " << x.rightOperands[0] << endl;
        }
        else if (x.opcode == "br") {
            outfile << x.opcode << " -> " << x.rightOperands[0] << endl;
        }
        else if (x.opcode == "cbr") {
            outfile << x.opcode << " " << x.leftOperands[0] << " -> " << x.rightOperands[0] << ", " << x.rightOperands[1] << endl;
        }
        else if (x.opcode == "not" || x.opcode == "loadI" || x.opcode == "load"
                 || x.opcode == "cload" || x.opcode == "store" || x.opcode == "cstore" || x.opcode == "i2i"
                 || x.opcode == "c2c" || x.opcode == "i2c" || x.opcode == "c2i") {
            outfile << x.opcode << " " << x.leftOperands[0] << " => " << x.rightOperands[0] << endl;
        }
        else if (x.opcode == "storeAI" || x.opcode == "storeAO" || x.opcode == "cstoreAI" || x.opcode == "cstoreAO") {
            outfile << x.opcode << " " << x.leftOperands[0] << " => " << x.rightOperands[0] << ", " << x.rightOperands[1] << endl;
        }
        else {
            outfile << x.opcode << " " << x.leftOperands[0] << ", " << x.leftOperands[1] << " => " << x.rightOperands[0] << endl;
        }

    }
    outfile.close();
}

void buildCFG(vector<int> &leader, vector<int> &last, vector<instruction> instructions) {
    leader.push_back(1);
    unordered_set<int> leadersSet;
    leadersSet.insert(1);
    for (auto x : instructions) {
        // br -> L2
        if (x.opcode == "br") {
            int findStartLine = label2line[x.rightOperands[0]];
            if (leadersSet.find(findStartLine) == leadersSet.end()) {
                leader.push_back(findStartLine);
                leadersSet.insert(findStartLine);
            }
        }
        // cbr r11 -> L3, L4
        else if (x.opcode == "cbr") {
            int findStartLine1 = label2line[x.rightOperands[0]];
            if (leadersSet.find(findStartLine1) == leadersSet.end()) {
                leader.push_back(findStartLine1);
                leadersSet.insert(findStartLine1);
            }

            int findStartLine2 = label2line[x.rightOperands[1]];
            if (leadersSet.find(findStartLine2) == leadersSet.end()) {
                leader.push_back(findStartLine2);
                leadersSet.insert(findStartLine2);
            }
        }
    }
    instruction instr = instructions.back();
    //find each leader's corresponding last
    int totalLineNum = instructions.size();
    for (int i = 0; i < leader.size(); i++) {
        int tmp = leader[i];
        while (tmp <= totalLineNum && leadersSet.find(tmp + 1) == leadersSet.end()) {
            tmp++;
        }
        last.push_back(tmp);
    }
}


void localValueNumbering(vector<int> leader, vector<int> last, vector<instruction> &instructions) {
    int basicBlockNum = leader.size();
    for (int i = 0; i < basicBlockNum; i++) {
        int begin = leader[i] - 1;
        int end = last[i] - 1;

        unordered_map <string, int> expr2VN;
        unordered_map <string, int> VN;
        unordered_map <int, string> name;
        int newValueNum = 0;

        for (int j = begin; j <= end; j++) {
            if (instructions[j].opcode == "halt") {
                break;
            }
            //do not need do LVN
            else if (instructions[j].opcode == "nop" || instructions[j].opcode == "not" || instructions[j].opcode == "store"
                || instructions[j].opcode == "storeAI" || instructions[j].opcode == "storeAO" || instructions[j].opcode == "cstore"
                || instructions[j].opcode == "cstoreAI" || instructions[j].opcode == "cstoreAO"  || instructions[j].opcode == "i2i"
                || instructions[j].opcode == "c2c" || instructions[j].opcode == "i2c" || instructions[j].opcode == "c2i"
                || instructions[j].opcode == "br" || instructions[j].opcode == "cbr"
                || instructions[j].opcode == "read" || instructions[j].opcode ==  "cread" || instructions[j].opcode == "output"
                || instructions[j].opcode == "coutput" || instructions[j].opcode == "write" || instructions[j].opcode == "cwrite"
                || instructions[j].opcode == "loadI" || instructions[j].opcode == "load" || instructions[j].opcode ==  "cload") {

                continue;
            }
            else {
                if (VN.find(instructions[j].leftOperands[0]) == VN.end()) {
                    VN[instructions[j].leftOperands[0]] = newValueNum++;
                }
                if (VN.find(instructions[j].leftOperands[1]) == VN.end()) {
                    VN[instructions[j].leftOperands[1]] = newValueNum++;
                }

                string expr = to_string(VN[instructions[j].leftOperands[0]]) + instructions[j].opcode + to_string(VN[instructions[j].leftOperands[1]]);
                if (expr2VN.find(expr) != expr2VN.end()) {
                    if (VN[name[expr2VN[expr]]] == expr2VN[expr]) {
                        instruction instr;
                        instr.opcode = "i2i";
                        instr.leftOperands.push_back(name[expr2VN[expr]]);
                        instr.rightOperands.push_back(instructions[j].rightOperands[0]);
                        instructions[j] = instr;
                    }
                    VN[instructions[j].rightOperands[0]] = expr2VN[expr];
                }
                else {
                    VN[instructions[j].rightOperands[0]] = newValueNum;
                    expr2VN[expr] = newValueNum;
                    name[newValueNum] = instructions[j].rightOperands[0];
                    newValueNum++;
                }
            }
        }

    }
}

void getNewValues(vector<instruction> instructions, int &newRegister, int &newLabel) {
    for (auto x : instructions) {
        if (x.label != "") {
            int length = x.label.length();
            string tmp_label = x.label.substr(1, length - 1);
            int tmpLabelValue = stoi(tmp_label);
            if (tmpLabelValue > newLabel) {
                newLabel = tmpLabelValue;
            }
        }
        if (x.leftOperands.size() != 0) {
            for (auto y : x.leftOperands) {
                if (y[0] != 'r') {
                    continue;
                }
                int length = y.length();
                string tmpRegister = y.substr(1, length -  1);
                int tmpRegisterVal = stoi(tmpRegister);
                if (tmpRegisterVal > newRegister) {
                    newRegister = tmpRegisterVal;
                }
            }
        }
        if (x.rightOperands.size() != 0) {
            for (auto z : x.rightOperands) {
                if (z[0] != 'r') {
                    continue;
                }
                int length = z.length();
                string tmpRegister = z.substr(1, length -  1);
                int tmpRegisterVal = stoi(tmpRegister);
                if (tmpRegisterVal > newRegister) {
                    newRegister = tmpRegisterVal;
                }
            }
        }
    }
}


void loopUnrolling(vector<int> leader, vector<int> last,vector<instruction> &instructions,
                   int newRegister, int newLabel) {

    vector<instruction> insertInstructions;

    int basicBlockNum = leader.size();
    for (int i = 0; i < basicBlockNum; i++) {
        int begin = leader[i] - 1;
        int end = last[i] - 1;

        if (instructions[end - 1].opcode == "halt") {
            for (int k = begin; k <= end - 1; k++) {
                insertInstructions.push_back(instructions[k]);
            }
            instructions.swap(insertInstructions);
            continue;
        }
        if (instructions[begin].label != "" && instructions[end].opcode == "cbr") {
            if (instructions[begin].label == instructions[end].rightOperands[0] && instructions[end - 2].opcode == "addI") {

                instruction instr1;
                instr1.opcode = "nop";
                instr1.label = instructions[begin].label;
                insertInstructions.push_back(instr1);


                instruction tmp1;
                tmp1.opcode = "sub";
                tmp1.leftOperands.push_back(instructions[end - 1].leftOperands[1]);
                tmp1.leftOperands.push_back(instructions[end - 1].leftOperands[0]);
                string new_r = "r" + to_string(++newRegister);
                tmp1.rightOperands.push_back(new_r);
                insertInstructions.push_back(tmp1);


                instruction instr2;
                instr2.opcode = "rshiftI";
                instr2.leftOperands.push_back(new_r);
                instr2.leftOperands.push_back("2");
                instr2.rightOperands.push_back(new_r);
                insertInstructions.push_back(instr2);

                instruction instr3;
                instr3.opcode = "lshiftI";
                instr3.leftOperands.push_back(new_r);
                instr3.leftOperands.push_back("2");
                instr3.rightOperands.push_back(new_r);
                insertInstructions.push_back(instr3);


                instruction tmp2;
                tmp2.opcode = "add";
                tmp2.leftOperands.push_back(instructions[end - 1].leftOperands[0]);
                tmp2.leftOperands.push_back(new_r);
                string new_reg = "r" + to_string(++newRegister);
                tmp2.rightOperands.push_back(new_reg);
                insertInstructions.push_back(tmp2);


                instruction instr4;
                instr4.opcode = "cbr";
                instr4.leftOperands.push_back(new_r);
                string newLabel1 = "L" + to_string(++newLabel);
                instr4.rightOperands.push_back(newLabel1);
                string newLabel2 = "L" + to_string(++newLabel);
                instr4.rightOperands.push_back(newLabel2);
                insertInstructions.push_back(instr4);

                instruction instr5;
                instr5.opcode = "nop";
                instr5.label = newLabel1;
                insertInstructions.push_back(instr5);

                for (int b = 0; b < 4; b++) {
                    for (int a = begin + 1; a <= end - 2; a++) {
                        insertInstructions.push_back(instructions[a]);
                    }
                }

                instruction instr6;
                if (instructions[end - 1].opcode == "cmp_LE") {
                    instr6.opcode = "cmp_LT";
                }
                else if (instructions[end - 1].opcode == "cmp_LT") {
                    instr6.opcode = "cmp_LE";
                }

                instr6.leftOperands.push_back(instructions[end - 1].leftOperands[0]);
                instr6.leftOperands.push_back(new_reg);
                instr6.rightOperands.push_back(instructions[end].leftOperands[0]);
                insertInstructions.push_back(instr6);

                instruction instr7;
                instr7.opcode = "cbr";
                instr7.leftOperands.push_back(instructions[end].leftOperands[0]);
                instr7.rightOperands.push_back(newLabel1);
                instr7.rightOperands.push_back(newLabel2);
                insertInstructions.push_back(instr7);

                instruction instr8;
                instr8.opcode = "nop";
                instr8.label = newLabel2;
                insertInstructions.push_back(instr8);

                insertInstructions.push_back(instructions[end - 1]);

                instruction instr9;
                instr9.opcode = "cbr";
                instr9.leftOperands.push_back(instructions[end].leftOperands[0]);
                string newLabel3 = "L" + to_string(++newLabel);
                instr9.rightOperands.push_back(newLabel3);
                instr9.rightOperands.push_back(instructions[end].rightOperands[1]);
                insertInstructions.push_back(instr9);

                instruction instr10;
                instr10.opcode = "nop";
                instr10.label = newLabel3;
                insertInstructions.push_back(instr10);

                for (int c = begin + 1; c <= end - 2; c++) {
                    insertInstructions.push_back(instructions[c]);
                }

                insertInstructions.push_back(instructions[end - 1]);
                insertInstructions.push_back(instr9);

            }
            else {
                for (int k = begin; k <= end; k++){
                    insertInstructions.push_back(instructions[k]);
                }
                continue;
            }
        }
        else {
            for (int k = begin; k <= end; k++){
                insertInstructions.push_back(instructions[k]);
            }
            continue;
        }

    }

}

void updateLineNumber(vector<instruction>& instructions, unordered_map<string, int>& label2line) {
    int lineNumber = 1;
    for (auto x : instructions) {
        x.lineNumber = lineNumber++;
        if (x.label != "") {
            label2line[x.label] = x.lineNumber;
        }
    }
}


int main(int argc, char** argv) {
    if (argc < 3) {
        cout << "wrong command line" << endl;
        return -1;
    }

    string runType = argv[1];
    string filename = argv[2];
    vector<instruction> instructions;
    ParseInstructions(filename, instructions);
    string outputfileName = filename.substr(0, filename.size() - 2) + "_" + runType[1] + ".i";

    if (runType == "-v") {
        vector<int> leader;
        vector<int> last;
        buildCFG(leader, last, instructions);
        localValueNumbering(leader, last, instructions);
        OutputParseResults(instructions, outputfileName);

    }
    else if (runType == "-l") {
        vector<int> leader2;
        vector<int> last2;
        buildCFG(leader2, last2, instructions);
        int newRegister = -1;
        int newLabel = -1;
        getNewValues(instructions, newRegister, newLabel);
        sort(leader2.begin(), leader2.end());
        sort(last2.begin(), last2.end());
        loopUnrolling(leader2, last2, instructions, newRegister, newLabel);
        OutputParseResults(instructions, outputfileName);

    }
    else if (runType == "-b") {

        vector<int> leader;
        vector<int> last;
        buildCFG(leader, last, instructions);
        localValueNumbering(leader, last, instructions);

        updateLineNumber(instructions, label2line);

        vector<int> leader2;
        vector<int> last2;
        buildCFG(leader2, last2, instructions);
        int newRegister = -1;
        int newLabel = -1;
        getNewValues(instructions, newRegister, newLabel);
        sort(leader2.begin(), leader2.end());
        sort(last2.begin(), last2.end());
        loopUnrolling(leader2, last2, instructions, newRegister, newLabel);
        OutputParseResults(instructions, outputfileName);


    }
    else {
        cout << "wrong parameters" << endl;
        cout << "only -v, -l, -b accept " << endl;
        return 0;
    }

    return 0;
}