%{ 
#include <stdio.h>
#include <string.h>
#define YYERROR_VERBOSE
extern FILE *yyin;
int yylineno;
int syntax_error = 0;

void yyerror (char const *s){
	++syntax_error;
	printf ("Lino No.: %d \n Erro Info: %s\n", yylineno, s);
}

%}



%token NAME CHARCONST NUMBER AND BY CHAR ELSE FOR IF INT NOT OR PROCEDURE READ THEN TO WHILE WRITE
%token '+' '-'
%token '*' '/' 
%token LT LE EQ NE GT GE
%token ':' ';' ',' '=' '{' '}' '[' ']' '(' ')'

%%
Procedure : PROCEDURE NAME '{' Decls Stmts '}';

Decls : Decls Decl ';'
	  | Decl ';' ;

Decl : Type SpecList ;

Type : INT
	 | CHAR ;

SpecList : SpecList ',' Spec
		 | Spec;

Spec : NAME
	 | NAME '[' Bounds ']' ;

Bounds : Bounds ',' Bound
	   | Bound ;

Bound : NUMBER ':' NUMBER ;

Stmts : Stmts Stmt
	  | Stmt ;

Stmt : Reference '=' Expr ';'
	 | '{' Stmts '}'
	 | WHILE '(' Bool ')' '{' Stmts '}'
	 | FOR NAME '=' Expr TO
   		Expr BY Expr '{' Stmts '}'

	 | IF '(' Bool ')' THEN Stmt
	 | IF '(' Bool ')' THEN WithElse ELSE Stmt
	 | '{' '}' { yyerror("syntax error: continuous 2 braces"); }
	 | error ';' { yyerrok; }
	 | READ Reference ';'
	 | WRITE Expr ';' ;


WithElse : Reference '=' Expr ';'
	     | '{' Stmts '}'
		 | WHILE '(' Bool ')' '{' Stmts '}'
		 | FOR NAME '=' Expr TO
   			Expr BY Expr '{' Stmts '}'
		 |IF '(' Bool ')' THEN WithElse ELSE WithElse
		 | READ Reference ';'
		 | WRITE Expr ';' ;


Bool : NOT OrTerm
	 | OrTerm ;

OrTerm : OrTerm OR AndTerm
	   | AndTerm ;

AndTerm : AndTerm AND RelExpr
		| RelExpr ;

RelExpr : RelExpr LT Expr
		| RelExpr LE Expr
		| RelExpr EQ Expr
		| RelExpr NE Expr
		| RelExpr GE Expr
		| RelExpr GT Expr
		| Expr;

Expr : Expr '+' Term
	 | Expr '-' Term
	 | Term;

Term : Term '*' Factor
	 | Term '/' Factor
	 | Factor ;
		Factor : '(' Expr ')'
	 | Reference
	 | NUMBER
	 | CHARCONST;

Reference : NAME
		  | NAME '[' Exprs ']';

Exprs : Expr ',' Exprs
	  | Expr;

%%



int main (int argc, char* argv[]) {
	for (int i = 1; i < argc; ++i) {//why index start from 1
		if (argc == 2) {  
			if (strcmp("-h", argv[1])) {
				FILE *fp;
				fp = fopen(argv[1], "r");
				yyin = fp;
				} 
			else {
				printf ("Help Info:\n");
				printf ("make : compile\n");
				printf ("clear : delete all the compiled info\n");
				return 0;
			}
		} 
		else {
			yyin = stdin;
		}
	}
	yyparse();
	if (syntax_error == 0) {
		printf ("Congratulations! You have passed this test\n");
	} else {
		printf ("There are: %d errors!\n", syntax_error);
	}
	return 0;
}

int yywrap() { return 1; } 
