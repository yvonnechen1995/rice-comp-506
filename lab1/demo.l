
%{ 
extern int yylineno;
#include "demo.tab.h"
%}

NAME [A-Za-z][A-Za-z0-9]*
NUMBER  [0-9][0-9]* 
CHARCONST '[A-Za-z0-9]'

%%
"and" {return AND;};
"by"  {return BY;};
"char"  {return CHAR;};
"else"  {return ELSE;};
"for"   {return FOR;};
"if"  {return IF;};
"int"  {return INT;};
"not"   {return NOT;};
"or"   {return OR;};
"procedure"   {return PROCEDURE;};
"read"  {return READ;};
"then"  {return THEN;};
"to"    {return TO;};
"while"   {return WHILE;};
"write"   {return WRITE;};

"+"   {return '+';};
"-"   {return '-';};
"*"   {return '*';};
"/"   {return '/';};
"<"   {return LT;};
"<="    {return LE;};
"=="    {return EQ;};
"!="    {return NE;};
">"   {return GT;};
">="    {return GE;};

":"   {return ':';};
";"   {return ';';};
","   {return ',';};
"="   {return '=';};
"{"   {return '{';};
"}"   {return '}';};
"["   {return '[';};
"]"   {return ']';};
"("   {return '(';};
")"   {return ')';};

{NAME} {return NAME;};
{NUMBER} {return NUMBER;}
{CHARCONST} {return CHARCONST;}

. { }
"//".* ;
[ \t] {}  
[\n] {yylineno++; }  

%%
