%option yylineno
%{
/* 在这里面加入C代码 */
int count = 0;
%}
/* definition */
Digit    [0-9]
NUMBER   {Digit}+
Letter   [A-Za-z]
NAME   (_|{Letter})({Letter}|{Digit}|_)*
%%

if|then|else|end|repeat|until|read|write|or|and|int|bool|char|while|do {
    printf("Line %d: (KEY, %s)\n", yylineno, yytext); }  /* keyword */

":="|"="|"<"|"+"|"-"|"/"|"("|"*"|")"|";"|"<="|">="|">"|"," {
    printf("Line %d: (SYM, %s)\n", yylineno, yytext); }              /* symbol */

'[^'\n]*'       { printf("Line %d: (STR, %s)\n", yylineno, yytext); }       /* string */

"{"[^}]*"}"     {}                                                          /* comment */

[0-9]+          { printf("Line %d: (NUM, %s)\n", yylineno, yytext); }       /* NUMBER */

[A-Za-z]([A-Za-z]|[0-9])*    { printf("Line %d: (ID, %s)\n", yylineno, yytext); } /* NAME */

[ \t]+          {}                                                  /* whiltespace */

\n              {}                                                  /* newline */

.               { if(*yytext == '{' || *yytext == '\'')
                    printf("Line %d: missing the right part of %s\n", yylineno, yytext);
                  else
                    printf("Line %d: illegal character %s\n", yylineno, yytext);
                  yyterminate();
                }                                                           /* others */
%%

int yywrap() {
    return 1;
}

void main(int argc, char** argv) {
    if(argc > 1)
        yyin = fopen(argv[1], "r");
    else
        yyin = stdin;
    yylex();
}
